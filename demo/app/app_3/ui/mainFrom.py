# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainFrom.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_mainForm(object):
    def setupUi(self, mainForm):
        mainForm.setObjectName("mainForm")
        mainForm.resize(1008, 625)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/images/logo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        mainForm.setWindowIcon(icon)
        mainForm.setStyleSheet("QLabel,QLineEdit,QPlainTextEdit,QTextEdit,QSpinBox,QGroupBox,QComboBox,QDateEdit,QTimeEdit,QDateTimeEdit,QSpinBox,QTreeView,QListView::pane{\n"
"color:#00466B;\n"
"}\n"
"\n"
"\n"
".QPushButton{\n"
"border-style:none;\n"
"border:1px solid #C0DCF2;\n"
"color:#00466B;\n"
"padding:5px;\n"
"min-height:15px;\n"
"border-radius:5px;\n"
"background:qlineargradient(spread:pad,x1:0,y1:0,x2:0,y2:1,stop:0 #DEF0FE,stop:1 #C0DEF6);\n"
"}\n"
"\n"
".QPushButton:hover{\n"
"background:qlineargradient(spread:pad,x1:0,y1:0,x2:0,y2:1,stop:0 #F2F9FF,stop:1 #DAEFFF);\n"
"}\n"
"\n"
".QPushButton:pressed{\n"
"background:qlineargradient(spread:pad,x1:0,y1:0,x2:0,y2:1,stop:0 #DEF0FE,stop:1 #C0DEF6);\n"
"}\n"
"\n"
".QPushButton:disabled{\n"
"color:#838383;\n"
"background:#F4F4F4;\n"
"}\n"
"\n"
"\n"
"QLineEdit,QTextEdit,QPlainTextEdit,QSpinBox{\n"
"border:1px solid #C0DCF2;\n"
"border-radius:5px;\n"
"padding:2px;\n"
"background:none;\n"
"selection-background-color:#DEF0FE;\n"
"selection-color:#00466B;\n"
"}\n"
"QTabBar::tab-bar{ \n"
"left:20px;\n"
"}\n"
"QTabBar::tab{ \n"
"padding:3px; margin:4px; color:#DCDCDC;  border:1px solid #00006b;\n"
"border-left-width:4px; border-right-width:4px; border-top-width:4px; border-bottom-width:4px; \n"
"background: rgb(0, 0, 91);\n"
"min-width:40px;\n"
"}\n"
"\n"
"QTabBar::tab:selected{ \n"
"    Margin-left:-4px;\n"
"    Margin-right:-4px;    \n"
"    Margin-top:-4px;\n"
"    Margin-bottom:-4px;    \n"
"\n"
"    font: 10pt;\n"
"    color: rgb(85, 255, 0);\n"
"}\n"
"\n"
"QTabBar::tab:hover{ \n"
"    Margin-left:-4px;\n"
"    Margin-right:-4px;    \n"
"    Margin-top:-4px;\n"
"    Margin-bottom:-4px;    \n"
"    background-color: rgb(23, 143, 255);\n"
"}\n"
"QTabBar::tab:!select{ \n"
"background: rgb(0, 0, 91);\n"
"}\n"
"\n"
"\n"
"QHeaderView::section{\n"
"border: none; \n"
"background-color: rgba(16, 35, 208, 100);\n"
"}\n"
"\n"
"QTableWidget{\n"
"background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(42, 44, 111, 255), stop:0.521368 rgba(28, 29, 73, 255));\n"
"border:none;\n"
"}\n"
"\n"
"QTabWidget::pane{\n"
"background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(42, 44, 111, 255), stop:0.521368 rgba(28, 29, 73, 255));\n"
"border:0px solid #C0DCF2;\n"
"selection-background-color:#F2F9FF;\n"
"selection-color:#00466B;\n"
"alternate-background-color:#DAEFFF;\n"
"}\n"
"\n"
"\n"
"QTableWidget::item{\n"
"background:none;\n"
"color: rgb(220, 220, 220);\n"
"}\n"
"\n"
"QTableWidget::item:hover{\n"
"background-color:rgb(92,188,227,200)\n"
"}\n"
"\n"
"QTableWidget::item:selected{\n"
"//background-color:#1B89A1\n"
"background-color:#f5f5f5\n"
"}\n"
"\n"
"QScrollBar:vertical{\n"
"background:#484848;\n"
"    color: rgb(255, 255, 127);\n"
"}\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"")
        self.verticalLayout = QtWidgets.QVBoxLayout(mainForm)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.searchLabel = QtWidgets.QLabel(mainForm)
        font = QtGui.QFont()
        font.setFamily("AcadEref")
        font.setPointSize(10)
        self.searchLabel.setFont(font)
        self.searchLabel.setStyleSheet("border: none;\n"
"color: rgb(220,220,220);")
        self.searchLabel.setObjectName("searchLabel")
        self.horizontalLayout.addWidget(self.searchLabel)
        self.searchlineEdit = QtWidgets.QLineEdit(mainForm)
        self.searchlineEdit.setStyleSheet("border: 1px solid gray;\n"
"border-radius: 4px;\n"
"padding: 0 8px;\n"
"background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1, stop:0 rgba(42, 44, 111, 255), stop:0.521368 rgba(28, 29, 73, 255));\n"
"color: rgb(255, 255, 255);\n"
"\n"
"\n"
"selection-background-color: darkgray;")
        self.searchlineEdit.setObjectName("searchlineEdit")
        self.horizontalLayout.addWidget(self.searchlineEdit)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.tabWidget = QtWidgets.QTabWidget(mainForm)
        self.tabWidget.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.tabWidget.setAutoFillBackground(False)
        self.tabWidget.setStyleSheet("")
        self.tabWidget.setObjectName("tabWidget")
        self.verticalLayout.addWidget(self.tabWidget)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.addButton = QtWidgets.QPushButton(mainForm)
        self.addButton.setObjectName("addButton")
        self.horizontalLayout_2.addWidget(self.addButton)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.delButton = QtWidgets.QPushButton(mainForm)
        self.delButton.setObjectName("delButton")
        self.horizontalLayout_2.addWidget(self.delButton)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem2)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(mainForm)
        self.tabWidget.setCurrentIndex(-1)
        QtCore.QMetaObject.connectSlotsByName(mainForm)

    def retranslateUi(self, mainForm):
        _translate = QtCore.QCoreApplication.translate
        mainForm.setWindowTitle(_translate("mainForm", "快捷工具箱"))
        self.searchLabel.setText(_translate("mainForm", "搜索:"))
        self.addButton.setText(_translate("mainForm", "添加"))
        self.delButton.setText(_translate("mainForm", "删除"))
import res_rc
