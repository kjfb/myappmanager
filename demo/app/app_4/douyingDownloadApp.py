from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import sys
from 抖音下载 import Ui_Form

class MWin(QWidget, Ui_Form):
    def __init__(self, parent=None):
        super(MWin, self).__init__(parent)
        self.setupUi(self)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MWin()
    try:
        qssfile = open("./qss/style.qss", encoding='utf-8')
        qssStyle = qssfile.read()
        qssfile.close()
        w.setStyleSheet(qssStyle)
    except Exception as e:
        print(e)
    w.show()
    sys.exit(app.exec_())
