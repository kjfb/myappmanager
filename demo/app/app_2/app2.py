import psutil
import time
import multiprocessing


def getMemory():
    data = psutil.virtual_memory()
    memory = str(int(round(data.percent))) + "%"
    return memory

def getCpu():
    cpu = str(round(psutil.cpu_percent(interval=1), 2)) + "%"
    return cpu

def getTmp():
    psutil.sensors_temperatures()
    tmp = str(round(psutil.cpu_percent(interval=1), 2)) + "%"
    return tmp


def showMemoryCpuUsage(refesh_time):
    while True:
        memory = getMemory()
        cpu = getCpu()
        print("CPU占用：%s \t 内存占用：%s" % (cpu, memory))
        time.sleep(refesh_time)

def main():
    while (True):
        memory = getMemory()
        cpu = getCpu()
        time.sleep(0.2)
        print("CPU占用：%s \t 内存占用：%s"%(cpu, memory))


if __name__ == "__main__":
    print(psutil.disk_usage("D:"))
    print(psutil.cpu_stats())
    # p = multiprocessing.Process(target=showMemoryCpuUsage, args=(0.2,))
    # p.start()
    # p.join()
    # print("over")
    # main()