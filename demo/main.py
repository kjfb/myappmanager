################################################################################
##
## BY: WANDERSON M.PIMENTA
## PROJECT MADE WITH: Qt Designer and PySide2
## V: 1.0.0
##
################################################################################
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import sys

# IMPORT FUNCTIONS
from ui_functions import *

# GUI FILE
from ui_main import Ui_MainWindow

# user GUI FILE
from app.app_2.app2main import *
from app.app_3.main import MWin
from app.CAN_SAMPLING_CALC.CAN_SAMPING_BOARD import CAN_SAMPING_WIN
from app.CodeEdit.code_editor import QCodeEditor
from app.app_tscancode.tscancode_C import SCANCODEMWin

class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # MOVE WINDOW
        def moveWindow(event):
            # RESTORE BEFORE MOVE
            if UIFunctions.returnStatus() == 1:
                UIFunctions.maximize_restore(self)

            # IF LEFT CLICK MOVE WINDOW
            if event.buttons() == Qt.LeftButton:
                self.move(self.pos() + event.globalPos() - self.dragPos)
                self.dragPos = event.globalPos()
                event.accept()

        # SET TITLE BAR
        self.ui.title_bar.mouseMoveEvent = moveWindow

        ## ==> SET UI DEFINITIONS
        UIFunctions.uiDefinitions(self)

        ## ADD User Custom Page
        # self.addUserPage(Ui_Form(), self.ui.stackedWidget, self.ui.top_menus)
        # self.addUserPage(mapp2MWin(), self.ui.stackedWidget, self.ui.top_menus)
        # self.addUserPageAPP(MWin(), self.ui.stackedWidget, self.ui.top_menus)
        self.addUserPageAPP(mapp2MWin(), self.ui.stackedWidget, self.ui.top_menus)
        self.addUserPageAPP(MWin(), self.ui.stackedWidget, self.ui.top_menus)
        self.addUserPageAPP(CAN_SAMPING_WIN(), self.ui.stackedWidget, self.ui.top_menus)
        self.addUserPageAPP(QCodeEditor(), self.ui.stackedWidget, self.ui.top_menus)
        self.addUserPageAPP(SCANCODEMWin(), self.ui.stackedWidget, self.ui.top_menus)

        # self.ui.stackedWidget.setCurrentIndex(self.ui.stackedWidget.count() - 1)
        self.ui.stackedWidget.setCurrentIndex(0)
        ## Bind page and button

        ## SHOW ==> MAIN WINDOW
        ########################################################################
        self.show()

    def enterEvent(self, evt):
        # print("鼠标进来了", self.y())
        if self.y() == (-1.0*(self.height()-2)):
        # if self.y() == -498:
        #     print(self.y())
            try:
                # 开启动画
                animation = QPropertyAnimation(self, b"pos", self)
                animation.setStartValue(QPoint(self.x(), self.y()))
                animation.setEndValue(QPoint(self.x(), 0))
                animation.setDuration(200)
                animation.start()
            except Exception as e:
                print(e)



    def leaveEvent(self, evt):
        # print("鼠标离开了", self.y())
        if self.y() <= 0:
            try:
                # 开启动画
                animation = QPropertyAnimation(self, b"pos", self)
                animation.setStartValue(QPoint(self.x(), self.y()))
                y = -1*(self.height()-2)
                # print("likai:", y)
                animation.setEndValue(QPoint(self.x(), y))
                animation.setDuration(200)
                animation.start()
            except Exception as e:
                print(e)


    ## ADD USer page
    def addUserPage(self, newfrome, stackedWidget, buttonmenus):
        newWidget = QtWidgets.QWidget()
        # newfrome = Ui_Form()
        newfrome.setupUi(newWidget)

        stackedWidget.addWidget(newWidget)

        # add pushbutton
        btn_page = QtWidgets.QPushButton(buttonmenus)
        btnobjname = "Btn"+ str(stackedWidget.count() - 1)
        print(stackedWidget.count())
        print(btnobjname)
        print(stackedWidget.count())
        btn_page.setObjectName(btnobjname)

        # btn_page.setText(btnobjname)
        btn_page.setStyleSheet(
            """
            color: rgb(85, 255, 0);
            font: 10pt;
        """
        )
        if stackedWidget.count() == 1:
            btn_page.setIcon(QIcon("images/icons_svg/icon_signal.svg"))
        else:
            btn_page.setIcon(QIcon("images/icons_svg/icon_search.svg"))
        self.ui.verticalLayout_10.addWidget(btn_page)
        # 创建按钮的时候 名称Btn 0 1 2 , 在绑定信号槽的时候 btn 和 stackedWidget index 关联
        btn_page.clicked.connect(lambda: self.ui.stackedWidget.setCurrentIndex(int(btn_page.objectName().strip("Btn"))))

    ## ADD USer page
    def addUserPageAPP(self, newWidget, stackedWidget, buttonmenus):

        stackedWidget.addWidget(newWidget)

        # add pushbutton
        btn_page = QPushButton(buttonmenus)
        btnobjname = "Btn" + str(stackedWidget.count() - 1)
        print(btnobjname)
        btn_page.setObjectName(btnobjname)

        # btn_page.setText(btnobjname)
        btn_page.setStyleSheet(
            """
            color: rgb(85, 255, 0);
            font: 10pt;
        """
        )

        if stackedWidget.count() == 1:
            btn_page.setIcon(QIcon("images/icons_svg/icon_signal.svg"))
        else:
            btn_page.setIcon(QIcon("images/icons_svg/icon_search.svg"))


        # 创建按钮的时候 名称Btn 0 1 2 , 在绑定信号槽的时候 btn 和 stackedWidget index 关联
        btn_page.clicked.connect(lambda: self.ui.stackedWidget.setCurrentIndex(int(btn_page.objectName().strip("Btn"))))

        self.ui.verticalLayout_10.addWidget(btn_page)

    ## APP EVENTS
    ########################################################################
    def mousePressEvent(self, event):
        self.dragPos = event.globalPos()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec_())
